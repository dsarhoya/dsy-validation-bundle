<?php

namespace dsarhoya\DSYValidationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RUTValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if(is_null($value) && $constraint->ignoreNull) return;
        if($value == '' && $constraint->ignoreNull) return;
        
        if (!$this->valida_rut($value)) {
            $this->context->addViolation($constraint->message);
        }
    }
    
    private function valida_rut($rut_con_verificador){
        if(is_int($rut_con_verificador)) {
            $rut_con_verificador = (string) $rut_con_verificador;
        }

        $rut_con_verificador = str_replace(".", "", $rut_con_verificador);
        $rut_con_verificador = str_replace(",", "", $rut_con_verificador);
        $rut_con_verificador = str_replace(" ", "", $rut_con_verificador);

        if (count(explode("-", $rut_con_verificador)) != 2) {
            return false;
        }

        $auxRut = explode("-", $rut_con_verificador);
        $rut = $auxRut[0];
        $verificador = $auxRut[1];

        $x = 2;
        $sumatorio = 0;
        for ($i = strlen($rut) - 1; $i >= 0; $i--) {
            if ($x > 7) {
                $x = 2;
            }
            $sumatorio = $sumatorio + ($rut[$i] * $x);
            $x++;
        }
        $digito = bcmod($sumatorio, 11);
        $digito = 11 - $digito;
        switch ($digito) {
            case 10:
                $digito = "K";
                break;
            case 11:
                $digito = "0";
                break;
        }
        
        if (strtolower($verificador) == strtolower($digito)) {
            return true;
        } else {
            return false;
        }
    }
}