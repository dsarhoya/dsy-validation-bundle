<?php

namespace dsarhoya\DSYValidationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NoDecimal extends Constraint
{
    public $message = 'El numero no debe tener decimales';
    
}