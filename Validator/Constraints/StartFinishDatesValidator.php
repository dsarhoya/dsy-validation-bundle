<?php

namespace dsarhoya\DSYValidationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class StartFinishDatesValidator extends ConstraintValidator
{
    public function validate($milestone, Constraint $constraint)
    {
        $startDate = 'get'.ucwords($constraint->startDate);
        $finishDate = 'get'.ucwords($constraint->finishDate);
        
        if(!$milestone->$startDate() || !$milestone->$finishDate()){
            $this->context->addViolationAt($constraint->errorPath ? $constraint->errorPath : $constraint->startDate, $constraint->message);
            return;
        }
        if ($milestone->$startDate() > $milestone->$finishDate()) {
            $this->context->addViolationAt($constraint->errorPath ? $constraint->errorPath : $constraint->startDate, $constraint->message);
        }
    }
}