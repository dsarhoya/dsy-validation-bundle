<?php

namespace dsarhoya\DSYValidationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NoDecimalValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!(is_numeric( $value ) && floor( $value ) == $value)) {
            $this->context->addViolation($constraint->message);
        }
    }
}