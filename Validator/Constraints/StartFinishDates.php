<?php

namespace dsarhoya\DSYValidationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class StartFinishDates extends Constraint
{
    public $message = 'La fecha inicial debe ser menor que la final';
    public $errorPath;
    public $startDate;
    public $finishDate;
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    
    public function getRequiredOptions()
    {
        return array('startDate', 'finishDate');
    }
}